#include <iostream>
#include <windows.h>
#include <wrl.h>
#include <functional>
#include <vector>

#include <d3d12.h>
#include <dxgi1_5.h>
#include <dxgidebug.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>

#include "../common/d3dx12.h"
#include "../common/window.h"
#include "../common/defines.h"

struct Vertex {
	DirectX::XMFLOAT3 pos;
};

// Window Variables
static HWND WINDOW_HANDLE = nullptr;
static LPCTSTR EXAMPLE_NAME = "Rendering a Triangle";
static unsigned int WIDTH = 640;
static unsigned int HEIGHT = 480;
static bool FULLSCREEN = false;

// Renderer Variables
const static unsigned int num_back_buffers = 3; // 2 = double buffering 3 = tripple buffering etc etc.
static unsigned int frame_index; // The current back buffer index. not the amount of frames rendered. (naming taken from the DX12 samples)
static IDXGIFactory5* dxgi_factory;
static ID3D12Device* device;
static IDXGISwapChain4* swap_chain;
static ID3D12CommandQueue* cmd_queue;

static ID3D12CommandAllocator** cmd_allocators;
static ID3D12GraphicsCommandList* cmd_list;

static ID3D12Resource* render_targets[num_back_buffers];
static ID3D12DescriptorHeap* rtv_descriptor_heap;
static unsigned int rtv_descriptor_increment_size;

static ID3D12Resource* depth_stencil_buffer;
static ID3D12DescriptorHeap* depth_stencil_heap;

static ID3D12Fence* fences[num_back_buffers];
static HANDLE fence_event;
static UINT64 fence_values[num_back_buffers];

static ID3D12RootSignature* root_signature;

static ID3D12Resource* vertex_buffer;
static int vertex_buffer_size;
static D3D12_VERTEX_BUFFER_VIEW vertex_buffer_view;

static ID3D12PipelineState* pipeline;
std::vector<D3D12_INPUT_ELEMENT_DESC> input_layout;

static D3D12_VIEWPORT viewport;
static D3D12_RECT scissor_rect;

static float clear_color[4] = { 0.568f, 0.733f, 1.0f, 1.0f };

// Window functions
void StartLoop(std::function<void()> init, std::function<void()> render);

// Debugging Functions
void EnableDebugLayer();
void ReportLiveObjects();

// Renderer Functions
void CreateDevice(IDXGIFactory5** out_factory, ID3D12Device** out_device);
void CreateSwapChain(IDXGISwapChain4** out_swap_chain);
void CreateDepthStencilHeap();
void CreateDepthStencilBuffer();
void CreateRenderTargetViews();
void CreateCommandList();
void CreateFences();
void CreateRootSignature();
void CreatePSO();
void CreateVertexBuffer();
void CreateViewport();
void InitD3D12();
void WaitForPrevFrame();

// Game Functions
void Init();
void Render();
