#include <../base.hpp>

#include <vector>
#include <DirectXMath.h>
#include <D3Dcompiler.h>
#include <iostream>

// Example Properties
const std::string tut::ExampleBase::name = "Example Base Test";
const bool tut::ExampleBase::allow_fullscreen = false;
const bool tut::ExampleBase::allow_resizing = false;
const std::uint16_t tut::ExampleBase::initial_width = 640;
const std::uint16_t tut::ExampleBase::initial_height = 360;
const std::uint8_t tut::ExampleBase::num_backbuffers = 3;
const D3D_FEATURE_LEVEL tut::ExampleBase::feature_level = D3D_FEATURE_LEVEL_11_0;

struct Vertex {
	DirectX::XMFLOAT3 pos;
};

class TestExample : public tut::ExampleBase {
private:
	ID3D12Resource* depth_stencil_buffer;
	ID3D12DescriptorHeap* depth_stencil_view_heap;

	std::array<ID3D12Resource*, num_backbuffers> render_targets;
	ID3D12DescriptorHeap* render_target_view_heap;

	ID3D12CommandAllocator** cmd_allocators;
	ID3D12GraphicsCommandList* cmd_list;

	ID3D12Fence* fences[num_backbuffers];
	HANDLE fence_event;
	UINT64 fence_values[num_backbuffers];

	ID3D12RootSignature* root_signature;

	ID3D12Resource* vertex_buffer;
	int vertex_buffer_size;
	D3D12_VERTEX_BUFFER_VIEW vertex_buffer_view;

	ID3D12Resource* index_buffer;
	int index_buffer_size;
	D3D12_INDEX_BUFFER_VIEW index_buffer_view;

	ID3D12PipelineState* pipeline;
	std::vector<D3D12_INPUT_ELEMENT_DESC> input_layout;

	D3D12_VIEWPORT viewport;
	D3D12_RECT scissor_rect;

	const float clear_color[4] = { 0.568f, 0.733f, 1.0f, 1.0f };

public:
	TestExample::~TestExample() {
		tut::SafeRelease(depth_stencil_buffer);
		tut::SafeRelease(depth_stencil_view_heap);
	}

	void CreateCommandLists() {
		cmd_allocators = new ID3D12CommandAllocator*[num_backbuffers];

		// Create the allocators
		for (int i = 0; i < num_backbuffers; i++) {
			HRESULT hr = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmd_allocators[i]));
			if (FAILED(hr)) {
				throw "Failed to create command allocator";
			}

			cmd_allocators[i]->SetName(L"CommandList allocator.");
		}

		// Create the command lists
		HRESULT hr = device->CreateCommandList(
			0,
			D3D12_COMMAND_LIST_TYPE_DIRECT,
			cmd_allocators[frame_idx],
			NULL,
			IID_PPV_ARGS(&cmd_list)
		);
		if (FAILED(hr)) {
			throw "Failed to create command list";
		}
		cmd_list->SetName(L"Native Commandlist");
	}

	void CreateFences() {
		HRESULT hr;

		// create the fences
		for (int i = 0; i < num_backbuffers; i++) {
			hr = device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fences[i]));
			if (FAILED(hr)) {
				throw "Failed to create fence.";
			}
			fence_values[i] = 0; // set the initial fence value to 0
		}

		// create a handle to a fence event
		fence_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (fence_event == nullptr) {
			throw "Failed to create fence event.";
		}
	}

	/*! \brief Creates a root signature.
	*/
	void CreateRootSignature() {
		D3D12_STATIC_SAMPLER_DESC sampler[1] = {};
		sampler[0].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
		sampler[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler[0].MipLODBias = 0;
		sampler[0].MaxAnisotropy = 0;
		sampler[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
		sampler[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
		sampler[0].MinLOD = 0.0f;
		sampler[0].MaxLOD = D3D12_FLOAT32_MAX;
		sampler[0].ShaderRegister = 0;
		sampler[0].RegisterSpace = 0;
		sampler[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

		CD3DX12_ROOT_SIGNATURE_DESC root_signature_desc;
		root_signature_desc.Init(0,
			nullptr, // a pointer to the beginning of our root parameters array
			1,
			sampler,
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

		ID3DBlob* signature;
		ID3DBlob* error = nullptr;
		HRESULT hr = D3D12SerializeRootSignature(&root_signature_desc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error); //TODO: FIX error parameter
		if (FAILED(hr)) {
			throw "Failed to create a serialized root signature";
		}

		hr = device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&root_signature));
		if (FAILED(hr)) {
			throw "Failed to create root signature";
		}
		root_signature->SetName(L"Native D3D12RootSignature");
	}

	/*! \brief Creates a Graphics Pipeline State Object
	*/
	void CreatePSO() {
		D3D12_BLEND_DESC blend_desc = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		D3D12_DEPTH_STENCIL_DESC depth_stencil_state = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		D3D12_RASTERIZER_DESC rasterize_desc = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		DXGI_SAMPLE_DESC sampleDesc = { 1, 0 };

		input_layout = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		};

		D3D12_INPUT_LAYOUT_DESC input_layout_desc = {};
		input_layout_desc.NumElements = input_layout.size();
		input_layout_desc.pInputElementDescs = input_layout.data();

		auto vertex_shader = tut::LoadShader("vertex.hlsl", "main", "vs_5_0");
		auto pixel_shader = tut::LoadShader("pixel.hlsl", "main", "ps_5_0");

		D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc = {};
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
		pso_desc.SampleDesc = sampleDesc;
		pso_desc.SampleMask = 0xffffffff;
		pso_desc.RasterizerState = rasterize_desc;
		pso_desc.BlendState = blend_desc;
		pso_desc.NumRenderTargets = 1;
		pso_desc.pRootSignature = root_signature;
		pso_desc.VS = vertex_shader.second;
		pso_desc.PS = pixel_shader.second;
		pso_desc.InputLayout = input_layout_desc;

		HRESULT hr = device->CreateGraphicsPipelineState(&pso_desc, IID_PPV_ARGS(&pipeline));
		if (FAILED(hr)) {
			throw "Failed to create graphics pipeline";
		}
		pipeline->SetName(L"My sick pipeline object");
	}

	/*! \brief Creates a vertex buffer contianing a rectangle.
	*   Requires a upload capable command list to be open.
	*/
	void CreateVertexBuffer() {
		// Rectangle Vertices
		Vertex vertices[] = {
			{ { -0.5f,  0.5f, 0.5f } },
			{ { 0.5f, -0.5f, 0.5f } },
			{ { -0.5f, -0.5f, 0.5f } },
			{ { 0.5f,  0.5f, 0.5f } }
		};

		vertex_buffer_size = sizeof(vertices);

		device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(vertex_buffer_size),
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&vertex_buffer));

		vertex_buffer->SetName(L"Vertex Buffer Resource Heap");

		ID3D12Resource* vb_upload_heap;
		device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(vertex_buffer_size),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&vb_upload_heap));

		vb_upload_heap->SetName(L"Vertex Buffer Upload Resource Heap");

		// store vertex buffer in upload heap
		D3D12_SUBRESOURCE_DATA vertex_data = {};
		vertex_data.pData = reinterpret_cast<BYTE*>(vertices);
		vertex_data.RowPitch = vertex_buffer_size;
		vertex_data.SlicePitch = vertex_buffer_size;

		UpdateSubresources(cmd_list, vertex_buffer, vb_upload_heap, 0, 0, 1, &vertex_data);

		// transition the vertex buffer data from copy destination state to vertex buffer state
		cmd_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(vertex_buffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));

		// create a vertex buffer view for the rectangle. We get the GPU memory address to the vertex buffer using the GetGPUVirtualAddress() method
		vertex_buffer_view.BufferLocation = vertex_buffer->GetGPUVirtualAddress();
		vertex_buffer_view.StrideInBytes = sizeof(Vertex);
		vertex_buffer_view.SizeInBytes = vertex_buffer_size;
	}

	/*! \brief Creates a vertex buffer contianing the indices of a rectangle.
	*   Requires a upload capable command list to be open.
	*/
	void CreateIndexBuffer() {
		// Rectangle Vertices
		uint32_t indices[] = {
			0, 1, 2, // first triangle
			0, 3, 1, // second triangle
		};

		index_buffer_size = sizeof(indices);

		device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(index_buffer_size),
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&index_buffer));

		index_buffer->SetName(L"Index Buffer Resource Heap");

		ID3D12Resource* ib_upload_heap;
		device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(index_buffer_size),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&ib_upload_heap));
		ib_upload_heap->SetName(L"Index Buffer Upload Resource Heap");

		// store index buffer in upload heap
		D3D12_SUBRESOURCE_DATA index_data = {};
		index_data.pData = reinterpret_cast<BYTE*>(indices);
		index_data.RowPitch = index_buffer_size;
		index_data.SlicePitch = index_buffer_size;

		UpdateSubresources(cmd_list, index_buffer, ib_upload_heap, 0, 0, 1, &index_data);

		// transition the index buffer data from copy destination state to index buffer state
		cmd_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(index_buffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER));

		// create a index buffer view for the rectangle. We get the GPU memory address to the index buffer using the GetGPUVirtualAddress() method
		index_buffer_view.BufferLocation = index_buffer->GetGPUVirtualAddress();
		index_buffer_view.SizeInBytes = index_buffer_size;
		index_buffer_view.Format = DXGI_FORMAT_R32_UINT;
	}

	/*! \brief Defines the viewport and scissor rect.
	*/
	void CreateViewport(tut::IVec2 size) {
		// Define viewport.
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.Width = size.x;
		viewport.Height = size.y;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;

		// Define scissor rect
		scissor_rect.left = 0;
		scissor_rect.top = 0;
		scissor_rect.right = size.x;
		scissor_rect.bottom = size.y;
	}

	void WaitForPrevFrame() {
		if (fences[frame_idx]->GetCompletedValue() < fence_values[frame_idx]) {
			// we have the fence create an event which is signaled once the fence's current value is "fenceValue"
			HRESULT hr = fences[frame_idx]->SetEventOnCompletion(fence_values[frame_idx], fence_event);
			if (FAILED(hr)) {
				throw "Failed to set fence event.";
			}

			WaitForSingleObject(fence_event, INFINITE);
		}

		// increment fenceValue for next frame
		fence_values[frame_idx]++;
	}

	virtual void Init() {
		render_targets = tut::GetRenderTargetsFromSwapChain<num_backbuffers>(device, swap_chain);
		render_target_view_heap = tut::CreateRenderTargetViewHeap(device, num_backbuffers);
		tut::CreateRTVsFromResourceArray(device, render_targets, render_target_view_heap->GetCPUDescriptorHandleForHeapStart());

		depth_stencil_view_heap = tut::CreateDepthStencilHeap(device, 1);
		depth_stencil_buffer = tut::CreateDepthStencilBuffer(device, depth_stencil_view_heap->GetCPUDescriptorHandleForHeapStart(), GetWindowSize());

		CreateCommandLists();
		CreateFences();
		CreateRootSignature();
		CreatePSO();
		CreateViewport(GetWindowSize());

		// Start recording
		CreateVertexBuffer();
		CreateIndexBuffer();

		// Now we execute the command list to upload the initial assets (triangle data)
		cmd_list->Close();
		ID3D12CommandList* ppCommandLists[] = { cmd_list };
		cmd_queue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

		// increment the fence value now, otherwise the buffer might not be uploaded by the time we start drawing
		fence_values[frame_idx]++;
		HRESULT hr = cmd_queue->Signal(fences[frame_idx], fence_values[frame_idx]);
		if (FAILED(hr)) {
			throw "Failed to signal command queue.";
		}
	}

	virtual void Render() {
		WaitForPrevFrame();

		// Reset command allocators and buffers
		HRESULT hr = cmd_allocators[frame_idx]->Reset();
		if (FAILED(hr)) {
			throw "Failed to reset cmd allocators";
		}

		// Only reset with pipeline state if using bundles since only then this will impact fps.
		// Otherwise its just easier to pass NULL and suffer the insignificant performance loss.
		hr = cmd_list->Reset(cmd_allocators[frame_idx], pipeline);

		if (FAILED(hr)) {
			throw "Failed to reset command list";
		}

		// Begin the command list.
		CD3DX12_RESOURCE_BARRIER begin_transition = CD3DX12_RESOURCE_BARRIER::Transition(
			render_targets[frame_idx],
			D3D12_RESOURCE_STATE_PRESENT,
			D3D12_RESOURCE_STATE_RENDER_TARGET
		);
		cmd_list->ResourceBarrier(1, &begin_transition);

		// Populate Command List
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(render_target_view_heap->GetCPUDescriptorHandleForHeapStart(), frame_idx, rtv_increment_size);
		CD3DX12_CPU_DESCRIPTOR_HANDLE dsv_handle(depth_stencil_view_heap->GetCPUDescriptorHandleForHeapStart());
		cmd_list->OMSetRenderTargets(1, &rtv_handle, false, &dsv_handle);
		cmd_list->ClearRenderTargetView(rtv_handle, clear_color, 0, nullptr);
		cmd_list->ClearDepthStencilView(dsv_handle, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

		cmd_list->SetPipelineState(pipeline);
		cmd_list->SetGraphicsRootSignature(root_signature);

		cmd_list->RSSetViewports(1, &viewport); // set the viewports
		cmd_list->RSSetScissorRects(1, &scissor_rect); // set the scissor rects

		cmd_list->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST); // set the primitive topology
		cmd_list->IASetVertexBuffers(0, 1, &vertex_buffer_view); // set the vertex buffer (using the vertex buffer view)
		cmd_list->IASetIndexBuffer(&index_buffer_view); // set the index buffer (using the index buffer view)

		cmd_list->DrawIndexedInstanced(6, 1, 0, 0, 0); // finally draw 4 vertices (draw the rectangle)

													   // Close and transition the cmd list
		CD3DX12_RESOURCE_BARRIER end_transition = CD3DX12_RESOURCE_BARRIER::Transition(
			render_targets[frame_idx],
			D3D12_RESOURCE_STATE_RENDER_TARGET,
			D3D12_RESOURCE_STATE_PRESENT
		);
		cmd_list->ResourceBarrier(1, &end_transition);
		cmd_list->Close();

		// execute the array of command lists
		ID3D12CommandList** cmd_lists = new ID3D12CommandList*[1];
		cmd_lists[0] = cmd_list;
		cmd_queue->ExecuteCommandLists(1, cmd_lists);

		// GPU Signal
		hr = cmd_queue->Signal(fences[frame_idx], fence_values[frame_idx]);
		if (FAILED(hr)) {
			throw "Failed to set fence signal.";
		}
	}
};

APP_ENTRY(TestExample)
