#include <iostream>
#include <windows.h>
#include <functional>

#include "../common/defines.h"

// Window variables
static HWND WINDOW_HANDLE = nullptr;
static LPCTSTR EXAMPLE_NAME= "Creating A Window";
static unsigned int WIDTH = 640;
static unsigned int HEIGHT = 480;
static bool FULLSCREEN = false;

// Window functions
HWND NODISCARD InitWindow(HINSTANCE inst, int show_cmd, int width, int height, bool fullscreen);
LRESULT CALLBACK WindowProc(HWND hWnd,	UINT msg, WPARAM w_param, LPARAM l_param);
void StartLoop(std::function<void()> init, std::function<void()> render);

// Game functions
void Init();
void Render();
