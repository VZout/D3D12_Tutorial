#include "main.h"

/*! \brief Creates a window.
*
*  This function creates a window using `CreateWindowEx` from windows.
* \param inst A handle to the instance of the module to be associated with the window. Obtained from the WinMain entry point.
* \param show_cmd Controls how the window is to be shown. Obtained from the WinMain entry point.
* \param width The width of the window.
* \param height The height of the window.
* \param fullscreen Determines whetever the window should be fullscreen.
*/
HWND InitWindow(HINSTANCE inst, int show_cmd, int width, int height, bool fullscreen) {
	HWND hwnd = nullptr;

	if (fullscreen) {
		HMONITOR hmon = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
		MONITORINFO mi = { sizeof(mi) };
		GetMonitorInfo(hmon, &mi);

		width = mi.rcMonitor.right - mi.rcMonitor.left;
		height = mi.rcMonitor.bottom - mi.rcMonitor.top;
	}

	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.cbClsExtra = NULL;
	wc.cbWndExtra = NULL;
	wc.hInstance = inst;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 2);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = EXAMPLE_NAME;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
		throw("Failed to register class with error: " + GetLastError());

	hwnd = CreateWindowEx(NULL,
		EXAMPLE_NAME, EXAMPLE_NAME,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		width, height,
		NULL, 
		NULL,
		inst,
		NULL);

	if (!hwnd)
		throw("Failed to create window with error: " + GetLastError());

	if (fullscreen) {
		SetWindowLong(hwnd, GWL_STYLE, 0);
	}

	ShowWindow(hwnd, show_cmd);
	UpdateWindow(hwnd);

	return hwnd;
}

/*! \brief The windows event callback.
*
*  This function receives a message and parameters from the window. You can use this for input handling as well.
* \param handle A handle to the window.
* \param msg The message. You can find a list of all messages here: https://msdn.microsoft.com/en-us/library/windows/desktop/ms644927(v=vs.85).aspx#system_defined
* \param w_param Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
* \param l_param Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
*/
LRESULT CALLBACK WindowProc(HWND handle, UINT msg, WPARAM w_param, LPARAM l_param) {
	switch (msg) {
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		case WM_KEYDOWN:
        	if (w_param == VK_ESCAPE)
                DestroyWindow(WINDOW_HANDLE);
        	return 0;
			
	}

	return DefWindowProc(handle, msg, w_param, l_param);
}

/*! \brief Starts the game's loop.
*
*  This function starts a infinite loop that breaks when the window is asked to close.
* \param init A function pointer to the functtion you want to use for initializing the game. This function gets called before the loop starts
* \param render A function pointer to the functtion you want to use for rendering the game. This function gets called every frame.
*/
void StartLoop(std::function<void()> init, std::function<void()> render) {
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	init();

	while (true) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			render();
		}
	}
}

/*! \brief Used for initializing the game.
*/
void Init() {
}

/*! \brief Used for rendering the game.
*/
void Render() {

}

/*! \brief Application entry point.
*/
int WINAPI WinMain(HINSTANCE inst, HINSTANCE prev_inst, LPSTR cmd_line, int show_cmd) {

	ALLOC_DEBUG_CONSOLE

	try { WINDOW_HANDLE = InitWindow(inst, show_cmd, WIDTH, HEIGHT, FULLSCREEN); }
	CATCH_EXCEPTS

	StartLoop(&Init, &Render);

	return 0;
}
