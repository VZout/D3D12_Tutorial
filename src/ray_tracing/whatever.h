#pragma once

#include <d3d12.h>
#include <cstdint>

struct TempPixel
{
	std::uint16_t r;
	std::uint16_t g;
	std::uint8_t b;
	std::uint8_t a;
};