#include "main.h"

#include "wrl.h"
#include <dxgidebug.h>

/*! \brief Starts the game's loop.
*
*  This function starts a infinite loop that breaks when the window is asked to close.
* \param init A function pointer to the functtion you want to use for initializing the game. This function gets called before the loop starts
* \param render A function pointer to the functtion you want to use for rendering the game. This function gets called every frame.
*/
void StartLoop(std::function<void()> init, std::function<void()> render) {
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	init();

	while (true) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
		render();
	}
}

void EnableDebugLayer() {
	Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)))) {
		debugController->EnableDebugLayer();
	}
}

void ReportLiveObjects() {
	Microsoft::WRL::ComPtr<IDXGIDebug> dxgiControler;
	if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiControler)))) {
		//dxgiControler->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_FLAGS(DXGI_DEBUG_RLO_DETAIL | DXGI_DEBUG_RLO_IGNORE_INTERNAL));
	} 
}

/* \brief Creates a DXGI factory.
*/
void CreateFactory() {
	HRESULT hr = CreateDXGIFactory1(IID_PPV_ARGS(&dxgi_factory));
	if (FAILED(hr)) {
		throw "Failed to create DXGIFactory.";
	}
}

/* \brief Creates a d3d12 device.
* Creates a d3d12 with feature level 11.0.
* The adapter is being discarded after device creation since we won't be using it.
*/
void CreateDevice() {
	IDXGIAdapter1* adapter = nullptr;
	int adapterIndex = 0;

	// Find a compatible adapter.
	while (dxgi_factory->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND) {
		DXGI_ADAPTER_DESC1 desc;
		adapter->GetDesc1(&desc);

		// Skip software adapters.
		if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
			adapterIndex++;
			continue;
		}

		// Create a device to test if the adapter supports the specified feature level.
		HRESULT hr = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr);
		if (SUCCEEDED(hr))
			break;

		adapterIndex++;
	}

	if (adapter == nullptr) {
		throw "No comaptible adapter found.";
	}

	// Actually create the device.
	HRESULT hr = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&device));
	if (FAILED(hr)) {
		throw "Failed to create device.";
	}

	adapter->Release();
}

/* \brief Creates a d3d12 swap chain.
* Creates a swap chain with the same size as the window with format DXGI_FORMAT_B8G8R8A8_UNORM.
* Without antialiasing and it stores the current back buffer index to frame_index for safety.
*/
void CreateSwapChain() {
	// Describe multisampling capabilities.
	DXGI_SAMPLE_DESC sample_desc = {};
	sample_desc.Count = 1;
	sample_desc.Quality = 0;

	// Describe the swap chain
	DXGI_SWAP_CHAIN_DESC1 swap_chain_desc = {};
	swap_chain_desc.Width = WIDTH;
	swap_chain_desc.Height = HEIGHT;
	swap_chain_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swap_chain_desc.SampleDesc = sample_desc;
	swap_chain_desc.BufferCount = num_back_buffers;
	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swap_chain_desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	swap_chain_desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	IDXGISwapChain1* temp_swap_chain;
	HRESULT hr = dxgi_factory->CreateSwapChainForHwnd(
		cmd_queue,
		WINDOW_HANDLE,
		&swap_chain_desc,
		NULL,
		NULL,
		&temp_swap_chain
	);
	if (FAILED(hr))
		throw "Failed to create swap chain.";

	swap_chain = static_cast<IDXGISwapChain4*>(temp_swap_chain);
	frame_index = swap_chain->GetCurrentBackBufferIndex();
}

/*! \brief Creates render target views.
*/
void CreateRenderTargetViews() {
	// Create the back buffers descriptor heap.
	D3D12_DESCRIPTOR_HEAP_DESC back_buffer_heap_desc = {};
	back_buffer_heap_desc.NumDescriptors = num_back_buffers;
	back_buffer_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	back_buffer_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	HRESULT hr = device->CreateDescriptorHeap(&back_buffer_heap_desc, IID_PPV_ARGS(&rtv_descriptor_heap));
	if (FAILED(hr))
		throw "Failed to create descriptor heap.";

	rtv_descriptor_increment_size = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	// Create render target view with the handle to the heap descriptor.
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(rtv_descriptor_heap->GetCPUDescriptorHandleForHeapStart());
	for (unsigned int i = 0; i < num_back_buffers; i++) {
		hr = swap_chain->GetBuffer(i, IID_PPV_ARGS(&render_targets[i]));
		if (FAILED(hr))
			throw "Failed to get swap chain buffer.";

		device->CreateRenderTargetView(render_targets[i], nullptr, rtv_handle);

		rtv_handle.Offset(1, rtv_descriptor_increment_size);
	}
}

/*! \brief Creates fences, resets the fence values and creates a event.
*/
void CreateFences() {
	// create the fences
	for (int i = 0; i < num_back_buffers; i++) {
		HRESULT hr = device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fences[i]));
		if (FAILED(hr)) {
			throw "Failed to create fence.";
		}  
		fence_values[i] = 0; // set the initial fence value to 0
	}

	// create a handle to a fence event
	fence_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (fence_event == nullptr) {
		throw "Failed to create fence event.";
	}
}

/*! \brief Creates a command list and its allocators
*/
void CreateCommandList() {
	cmd_allocators = new ID3D12CommandAllocator*[num_back_buffers];

	// Create the allocators
	for (int i = 0; i < num_back_buffers; i++) {
		HRESULT hr = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,IID_PPV_ARGS(&cmd_allocators[i]));
		if (FAILED(hr)) {
			throw "Failed to create command allocator";
		}

		cmd_allocators[i]->SetName(L"CommandList allocator.");
	}

	// Create the command lists
	HRESULT hr = device->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		cmd_allocators[frame_index],
		NULL,
		IID_PPV_ARGS(&cmd_list)
	);
	if (FAILED(hr)) {
		throw "Failed to create command list";
	}
	cmd_list->SetName(L"Native Commandlist");
}

/*! \brief Initializes D3D12.
*/
void InitD3D12() {
	CreateFactory();
	CreateDevice();

	// Create a direct command queue.
	D3D12_COMMAND_QUEUE_DESC cmd_queue_desc = {};
	cmd_queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	HRESULT hr = device->CreateCommandQueue(&cmd_queue_desc, IID_PPV_ARGS(&cmd_queue));
	if (FAILED(hr))
		throw "Failed to create direct command queue.";

	CreateSwapChain();
	CreateRenderTargetViews();

	CreateCommandList();
	CreateFences();
}

/*! \brief Waits for the command queue to finish.
*/
void WaitForPrevFrame() {
	if (fences[frame_index]->GetCompletedValue() < fence_values[frame_index]) {
		// we have the fence create an event which is signaled once the fence's current value is "fenceValue"
		HRESULT hr = fences[frame_index]->SetEventOnCompletion(fence_values[frame_index], fence_event);
		if (FAILED(hr)) {
			throw "Failed to set fence event.";
		}

		WaitForSingleObject(fence_event, INFINITE);
	}

	// increment fenceValue for next frame
	fence_values[frame_index]++;
}

/*! \brief Used for initializing the game.
*/
void Init() {
	cmd_list->Close();
}

/*! \brief Used for rendering the game.
*/
void Render() {
	// Reset command allocators and buffers
	HRESULT hr = cmd_allocators[frame_index]->Reset();
	if (FAILED(hr)) {
		throw "Failed to reset cmd allocators";
	}

	// Only reset with pipeline state if using bundles since only then this will impact fps.
	// Otherwise its just easier to pass NULL and suffer the insignificant performance loss.
	hr = cmd_list->Reset(cmd_allocators[frame_index], NULL);

	if (FAILED(hr)) {
		throw "Failed to reset command list";
	}

	// Begin the command list.
	CD3DX12_RESOURCE_BARRIER begin_transition = CD3DX12_RESOURCE_BARRIER::Transition(
		render_targets[frame_index],
		D3D12_RESOURCE_STATE_PRESENT,
		D3D12_RESOURCE_STATE_RENDER_TARGET
	);
	cmd_list->ResourceBarrier(1, &begin_transition);

	// Populate Command List
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(rtv_descriptor_heap->GetCPUDescriptorHandleForHeapStart(), frame_index, rtv_descriptor_increment_size);
	cmd_list->OMSetRenderTargets(1, &rtv_handle, false, nullptr);
	cmd_list->ClearRenderTargetView(rtv_handle, clear_color, 0, nullptr);

	// Close and transition the cmd list
	CD3DX12_RESOURCE_BARRIER end_transition = CD3DX12_RESOURCE_BARRIER::Transition(
		render_targets[frame_index],
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		D3D12_RESOURCE_STATE_PRESENT
	);
	cmd_list->ResourceBarrier(1, &end_transition);
	cmd_list->Close();

	// execute the array of command lists
	ID3D12CommandList** cmd_lists = new ID3D12CommandList*[1];
	cmd_lists[0] = cmd_list;
	cmd_queue->ExecuteCommandLists(1, cmd_lists);

	// GPU Signal
	hr = cmd_queue->Signal(fences[frame_index], fence_values[frame_index]);
	if (FAILED(hr)) {
		throw "Failed to set fence signal.";
	}

	swap_chain->Present(0, 0);

	WaitForPrevFrame();

	// Update our frame index
	frame_index = swap_chain->GetCurrentBackBufferIndex();

}

/*! \brief Application entry point.
*/
int WINAPI WinMain(HINSTANCE inst, HINSTANCE prev_inst, LPSTR cmd_line, int show_cmd) {

	ALLOC_DEBUG_CONSOLE

	Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)))) {
		debugController->EnableDebugLayer();
	}

	try { WINDOW_HANDLE = sample1::InitWindow(EXAMPLE_NAME, inst, show_cmd, WIDTH, HEIGHT, FULLSCREEN); }
	CATCH_EXCEPTS

	EnableDebugLayer();

	try { InitD3D12(); }
	CATCH_EXCEPTS

	StartLoop(&Init, &Render);

	ReportLiveObjects();

	return 0;
}
