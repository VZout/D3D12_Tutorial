/*! \file defines.h
\brief Defines macros to simplfy the code.
*/

// Used for checking is a attribute is available.
#ifndef __has_cpp_attribute
#define __has_cpp_attribute(name) 0
#endif

// Define NODISCARD if available
#if __has_cpp_attribute(nodiscard)
#define NODISCARD [[nodiscard]]
#else
#define NODISCARD
#endif

// Catches exceptions thrown by my code.
#define CATCH_EXCEPTS catch(const char* e) { std::cout << "Exception: " << e << '\n'; }

// Allocates a console and redirect stdin, out and err for outputting debug information.
#ifdef _DEBUG
#define ALLOC_DEBUG_CONSOLE AllocConsole(); freopen("CONIN$", "r", stdin); freopen("CONOUT$", "w", stdout); freopen("CONOUT$", "w", stderr);
#else
#define ALLOC_DEBUG_CONSOLE
#endif