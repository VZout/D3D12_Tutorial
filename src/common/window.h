#pragma once

#include <windows.h>
#include <string> 

#include "../common/defines.h"

namespace sample1 {

	HWND NODISCARD InitWindow(const char* name, HINSTANCE inst, int show_cmd, int width, int height, bool fullscreen);
	LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM w_param, LPARAM l_param);

}