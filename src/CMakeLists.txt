function(buildExample EXAMPLE_NAME)
	SET(EXAMPLE_FOLDER ${CMAKE_CURRENT_SOURCE_DIR}/${EXAMPLE_NAME})
	message(STATUS "Configuring example ${EXAMPLE_NAME}")

	# source
	file(GLOB SOURCES "${EXAMPLE_NAME}/*.cpp")
	file(GLOB HEADERS "${EXAMPLE_NAME}/*.h")

	# shaders
	file(GLOB SHADERS "${CMAKE_CURRENT_SOURCE_DIR}/../*.hlsl")
	set_source_files_properties(${SHADERS} PROPERTIES VS_TOOL_OVERRIDE "None")
	#set_source_files_properties( ${PS_SHADERS} PROPERTIES VS_SHADER_TYPE Pixel VS_SHADER_MODEL 5.0 VS_SHADER_ENTRYPOINT main )
	#set_source_files_properties( ${VS_SHADERS} PROPERTIES VS_SHADER_TYPE Vertex VS_SHADER_MODEL 5.0 VS_SHADER_ENTRYPOINT main )

	add_executable(${EXAMPLE_NAME} WIN32 ${SOURCES} ${HEADERS} ${COMMON_SOURCES} ${COMMON_HEADERS} ${SHADERS})
	target_include_directories (${EXAMPLE_NAME} PUBLIC ${BASE_HEADERS})
	add_dependencies(${EXAMPLE_NAME} ExampleBase)
	target_link_libraries(${EXAMPLE_NAME} ExampleBase)
	add_dependencies(${EXAMPLE_NAME} ExampleBase)
	set_target_properties(${EXAMPLE_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../)
endfunction(buildExample)

function(buildExamples)
	foreach(EXAMPLE ${EXAMPLES})
		buildExample(${EXAMPLE})
	endforeach(EXAMPLE)
endfunction(buildExamples)

set(EXAMPLES
	2_initialize_d3d12
	3_render_triangle
	4_depth_buffer
	5_indexed_rendering
	6_constant_buffers
	ray_tracing
	test_example
)

buildExamples()
