#include "main.h"

/*! \brief Starts the game's loop.
*
*  This function starts a infinite loop that breaks when the window is asked to close.
* \param init A function pointer to the functtion you want to use for initializing the game. This function gets called before the loop starts
* \param render A function pointer to the functtion you want to use for rendering the game. This function gets called every frame.
*/
void StartLoop(std::function<void()> init, std::function<void()> render) {
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	init();

	while (true) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
		render();
	}
}

void EnableDebugLayer() {
	Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)))) {
		debugController->EnableDebugLayer();
	}
}

void ReportLiveObjects() {
	Microsoft::WRL::ComPtr<IDXGIDebug> dxgiControler;
	if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiControler)))) {
		//dxgiControler->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_FLAGS(DXGI_DEBUG_RLO_DETAIL | DXGI_DEBUG_RLO_IGNORE_INTERNAL));
	} 
}

void CreateDevice(IDXGIFactory5** out_factory, ID3D12Device** out_device) {
	HRESULT hr = CreateDXGIFactory1(IID_PPV_ARGS(out_factory));
	if (FAILED(hr)) {
		throw "Failed to create DXGIFactory.";
	}

	IDXGIAdapter1* adapter = nullptr;
	int adapterIndex = 0;

	// Find a compatible adapter.
	while ((*out_factory)->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND) {
		DXGI_ADAPTER_DESC1 desc;
		adapter->GetDesc1(&desc);

		// Skip software adapters.
		if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
			adapterIndex++;
			continue;
		}

		// Create a device to test if the adapter supports the specified feature level.
		hr = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr);
		if (SUCCEEDED(hr))
			break;

		adapterIndex++;
	}

	if (adapter == nullptr) {
		throw "No comaptible adapter found.";
	}

	// Actually create the device.
	hr = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(out_device));
}

void CreateSwapChain(IDXGISwapChain4** out_swap_chain) {
	// Describe multisampling capabilities.
	DXGI_SAMPLE_DESC sample_desc = {};
	sample_desc.Count = 1;
	sample_desc.Quality = 0;

	// Describe the swap chain
	DXGI_SWAP_CHAIN_DESC1 swap_chain_desc = {};
	swap_chain_desc.Width = WIDTH;
	swap_chain_desc.Height = HEIGHT;
	swap_chain_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swap_chain_desc.SampleDesc = sample_desc;
	swap_chain_desc.BufferCount = num_back_buffers;
	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swap_chain_desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	swap_chain_desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	IDXGISwapChain1* temp_swap_chain;
	HRESULT hr = dxgi_factory->CreateSwapChainForHwnd(
		cmd_queue,
		WINDOW_HANDLE,
		&swap_chain_desc,
		NULL,
		NULL,
		&temp_swap_chain
	);
	if (FAILED(hr))
		throw "Failed to create swap chain.";

	*out_swap_chain = static_cast<IDXGISwapChain4*>(temp_swap_chain);
	frame_index = (*out_swap_chain)->GetCurrentBackBufferIndex();
}

/*! \brief Creates render target views.
*/
void CreateRenderTargetViews() {
	// Create the back buffers descriptor heap.
	D3D12_DESCRIPTOR_HEAP_DESC back_buffer_heap_desc = {};
	back_buffer_heap_desc.NumDescriptors = num_back_buffers;
	back_buffer_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	back_buffer_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	HRESULT hr = device->CreateDescriptorHeap(&back_buffer_heap_desc, IID_PPV_ARGS(&rtv_descriptor_heap));
	if (FAILED(hr))
		throw "Failed to create descriptor heap.";

	rtv_descriptor_increment_size = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	// Create render target view with the handle to the heap descriptor.
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(rtv_descriptor_heap->GetCPUDescriptorHandleForHeapStart());
	for (unsigned int i = 0; i < num_back_buffers; i++) {
		hr = swap_chain->GetBuffer(i, IID_PPV_ARGS(&render_targets[i]));
		if (FAILED(hr))
			throw "Failed to get swap chain buffer.";

		device->CreateRenderTargetView(render_targets[i], nullptr, rtv_handle);

		rtv_handle.Offset(1, rtv_descriptor_increment_size);
	}
}

void CreateFences() {
	HRESULT hr;

	// create the fences
	for (int i = 0; i < num_back_buffers; i++) {
		hr = device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fences[i]));
		if (FAILED(hr)) {
			throw "Failed to create fence.";
		}  
		fence_values[i] = 0; // set the initial fence value to 0
	}

	// create a handle to a fence event
	fence_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (fence_event == nullptr) {
		throw "Failed to create fence event.";
	}
}

/*! \brief Creates a root signature.
*/
void CreateRootSignature() {
	D3D12_STATIC_SAMPLER_DESC sampler[1] = {};
	sampler[0].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
	sampler[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler[0].MipLODBias = 0;
	sampler[0].MaxAnisotropy = 0;
	sampler[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	sampler[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
	sampler[0].MinLOD = 0.0f;
	sampler[0].MaxLOD = D3D12_FLOAT32_MAX;
	sampler[0].ShaderRegister = 0;
	sampler[0].RegisterSpace = 0;
	sampler[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	CD3DX12_ROOT_SIGNATURE_DESC root_signature_desc;
	root_signature_desc.Init(0,
		nullptr, // a pointer to the beginning of our root parameters array
		1,
		sampler,
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ID3DBlob* signature;
	ID3DBlob* error = nullptr;
	HRESULT hr = D3D12SerializeRootSignature(&root_signature_desc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error); //TODO: FIX error parameter
	if (FAILED(hr)) {
		throw "Failed to create a serialized root signature";
	}

	hr = device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&root_signature));
	if (FAILED(hr)) {
		throw "Failed to create root signature";
	}
	root_signature->SetName(L"Native D3D12RootSignature");
}

/*! \brief Creates a Graphics Pipeline State Object
*/
void CreatePSO() {
	D3D12_BLEND_DESC blend_desc = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	D3D12_DEPTH_STENCIL_DESC depth_stencil_state = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	D3D12_RASTERIZER_DESC rasterize_desc = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	DXGI_SAMPLE_DESC sampleDesc = {1, 0};

	input_layout = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	D3D12_INPUT_LAYOUT_DESC input_layout_desc = {};
	input_layout_desc.NumElements = input_layout.size();
	input_layout_desc.pInputElementDescs = input_layout.data();

	/*##########################
	BEGIN LOAD SHADERS
	########################*/
	ID3DBlob* vs;
	ID3DBlob* error;
	HRESULT hr = D3DCompileFromFile(L"vertex.hlsl",
		nullptr,
		nullptr,
		"main",
		"vs_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&vs,
		&error);
	if (FAILED(hr)) {
		throw((char*)error->GetBufferPointer());
	}

	ID3DBlob* ps; // d3d blob for holding vertex shader bytecode
	hr = D3DCompileFromFile(L"pixel.hlsl",
		nullptr,
		nullptr,
		"main",
		"ps_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&ps,
		&error);
	if (FAILED(hr)) {
		throw((char*)error->GetBufferPointer());
	}
	/*##########################
	END LOAD SHADERS
	########################*/

	D3D12_SHADER_BYTECODE vs_bytecode = {};
	vs_bytecode.BytecodeLength = vs->GetBufferSize();
	vs_bytecode.pShaderBytecode = vs->GetBufferPointer();

	D3D12_SHADER_BYTECODE ps_bytecode = {};
	ps_bytecode.BytecodeLength = ps->GetBufferSize();
	ps_bytecode.pShaderBytecode = ps->GetBufferPointer();

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc = {};
	pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pso_desc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
	//pso_desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	pso_desc.SampleDesc = sampleDesc;
	pso_desc.SampleMask = 0xffffffff;
	pso_desc.RasterizerState = rasterize_desc;
	pso_desc.BlendState = blend_desc;
	//pso_desc.DepthStencilState = depth_stencil_state;
	pso_desc.NumRenderTargets = 1;
	pso_desc.pRootSignature = root_signature;
	pso_desc.VS = vs_bytecode;
	pso_desc.PS = ps_bytecode;
	pso_desc.InputLayout = input_layout_desc;

	hr = device->CreateGraphicsPipelineState(&pso_desc, IID_PPV_ARGS(&pipeline));
	if (FAILED(hr)) {
		throw "Failed to create graphics pipeline";
	}
	pipeline->SetName(L"My sick pipeline object");
}

/*! \brief Creates a vertex buffer contianing a triangle.
*   Requires a upload capable command list to be open.
*/
void CreateVertexBuffer() {
	// Triangle Vertices
	Vertex vertices[] = {
		{ { 0.0f, 0.5f, 0.5f } },
		{ { 0.5f, -0.5f, 0.5f } },
		{ { -0.5f, -0.5f, 0.5f } },
	};

	vertex_buffer_size = sizeof(vertices);

	device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(vertex_buffer_size),
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&vertex_buffer));

	vertex_buffer->SetName(L"Vertex Buffer Resource Heap");

	ID3D12Resource* vb_upload_heap;
	device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(vertex_buffer_size),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vb_upload_heap));
	vb_upload_heap->SetName(L"Vertex Buffer Upload Resource Heap");

	// store vertex buffer in upload heap
	D3D12_SUBRESOURCE_DATA vertex_data = {};
	vertex_data.pData = reinterpret_cast<BYTE*>(vertices);
	vertex_data.RowPitch = vertex_buffer_size;
	vertex_data.SlicePitch = vertex_buffer_size;

	UpdateSubresources(cmd_list, vertex_buffer, vb_upload_heap, 0, 0, 1, &vertex_data);

	// transition the vertex buffer data from copy destination state to vertex buffer state
	cmd_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(vertex_buffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));
}

/*! \brief Defines the viewport and scissor rect.
*/
void CreateViewport() {
	// Define viewport.
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = WIDTH;
	viewport.Height = HEIGHT;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	// Define scissor rect
	scissor_rect.left = 0;
	scissor_rect.top = 0;
	scissor_rect.right = WIDTH;
	scissor_rect.bottom = HEIGHT;
}

/*! \brief Creates a command list and its allocators
*/
void CreateCommandList() {
	cmd_allocators = new ID3D12CommandAllocator*[num_back_buffers];

	// Create the allocators
	for (int i = 0; i < num_back_buffers; i++) {
		HRESULT hr = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,IID_PPV_ARGS(&cmd_allocators[i]));
		if (FAILED(hr)) {
			throw "Failed to create command allocator";
		}

		cmd_allocators[i]->SetName(L"CommandList allocator.");
	}

	// Create the command lists
	HRESULT hr = device->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		cmd_allocators[frame_index],
		NULL,
		IID_PPV_ARGS(&cmd_list)
	);
	if (FAILED(hr)) {
		throw "Failed to create command list";
	}
	cmd_list->SetName(L"Native Commandlist");
}

/*! \brief Initializes D3D12.
*/
void InitD3D12() {
	CreateDevice(&dxgi_factory, &device);

	// Create a direct command queue.
	D3D12_COMMAND_QUEUE_DESC cmd_queue_desc = {};
	cmd_queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	HRESULT hr = device->CreateCommandQueue(&cmd_queue_desc, IID_PPV_ARGS(&cmd_queue));
	if (FAILED(hr))
		throw "Failed to create direct command queue.";

	CreateSwapChain(&swap_chain);
	CreateRenderTargetViews();

	CreateCommandList();
	CreateFences();
}

/*! \brief Waits for the command queue to finish.
*/
void WaitForPrevFrame() {
	if (fences[frame_index]->GetCompletedValue() < fence_values[frame_index]) {
		// we have the fence create an event which is signaled once the fence's current value is "fenceValue"
		HRESULT hr = fences[frame_index]->SetEventOnCompletion(fence_values[frame_index], fence_event);
		if (FAILED(hr)) {
			throw "Failed to set fence event.";
		}

		WaitForSingleObject(fence_event, INFINITE);
	}

	// increment fenceValue for next frame
	fence_values[frame_index]++;
}

/*! \brief Used for initializing the game.
*/
void Init() {
	CreateRootSignature();
	CreatePSO();

	CreateVertexBuffer();
	CreateViewport();

	// Now we execute the command list to upload the initial assets (triangle data)
	cmd_list->Close();
	ID3D12CommandList* ppCommandLists[] = { cmd_list };
	cmd_queue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// increment the fence value now, otherwise the buffer might not be uploaded by the time we start drawing
	fence_values[frame_index]++;
	HRESULT hr = cmd_queue->Signal(fences[frame_index], fence_values[frame_index]);
	if (FAILED(hr)) {
		throw "Failed to signal command queue.";
	}

	// create a vertex buffer view for the triangle. We get the GPU memory address to the vertex pointer using the GetGPUVirtualAddress() method
	vertex_buffer_view.BufferLocation = vertex_buffer->GetGPUVirtualAddress();
	vertex_buffer_view.StrideInBytes = sizeof(Vertex);
	vertex_buffer_view.SizeInBytes = vertex_buffer_size;
}

/*! \brief Used for rendering the game.
*/
void Render() {
	// Reset command allocators and buffers
	HRESULT hr = cmd_allocators[frame_index]->Reset();
	if (FAILED(hr)) {
		throw "Failed to reset cmd allocators";
	}

	// Only reset with pipeline state if using bundles since only then this will impact fps.
	// Otherwise its just easier to pass NULL and suffer the insignificant performance loss.
	hr = cmd_list->Reset(cmd_allocators[frame_index], pipeline);

	if (FAILED(hr)) {
		throw "Failed to reset command list";
	}

	// Begin the command list.
	CD3DX12_RESOURCE_BARRIER begin_transition = CD3DX12_RESOURCE_BARRIER::Transition(
		render_targets[frame_index],
		D3D12_RESOURCE_STATE_PRESENT,
		D3D12_RESOURCE_STATE_RENDER_TARGET
	);
	cmd_list->ResourceBarrier(1, &begin_transition);

	// Populate Command List
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(rtv_descriptor_heap->GetCPUDescriptorHandleForHeapStart(), frame_index, rtv_descriptor_increment_size);
	cmd_list->OMSetRenderTargets(1, &rtv_handle, false, nullptr);
	cmd_list->ClearRenderTargetView(rtv_handle, clear_color, 0, nullptr);

	cmd_list->SetPipelineState(pipeline);
	cmd_list->SetGraphicsRootSignature(root_signature);

	cmd_list->RSSetViewports(1, &viewport); // set the viewports
	cmd_list->RSSetScissorRects(1, &scissor_rect); // set the scissor rects

	cmd_list->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST); // set the primitive topology
	cmd_list->IASetVertexBuffers(0, 1, &vertex_buffer_view); // set the vertex buffer (using the vertex buffer view)

	cmd_list->DrawInstanced(3, 1, 0, 0); // finally draw 3 vertices (draw the triangle)

	// Close and transition the cmd list
	CD3DX12_RESOURCE_BARRIER end_transition = CD3DX12_RESOURCE_BARRIER::Transition(
		render_targets[frame_index],
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		D3D12_RESOURCE_STATE_PRESENT
	);
	cmd_list->ResourceBarrier(1, &end_transition);
	cmd_list->Close();

	// execute the array of command lists
	ID3D12CommandList** cmd_lists = new ID3D12CommandList*[1];
	cmd_lists[0] = cmd_list;
	cmd_queue->ExecuteCommandLists(1, cmd_lists);

	// GPU Signal
	hr = cmd_queue->Signal(fences[frame_index], fence_values[frame_index]);
	if (FAILED(hr)) {
		throw "Failed to set fence signal.";
	}

	swap_chain->Present(0, 0);

	WaitForPrevFrame();

	// Update our frame index
	frame_index = swap_chain->GetCurrentBackBufferIndex();

}

#include "wrl.h"
#include <dxgidebug.h>

/*! \brief Application entry point.
*/
int WINAPI WinMain(HINSTANCE inst, HINSTANCE prev_inst, LPSTR cmd_line, int show_cmd) {

	ALLOC_DEBUG_CONSOLE

	Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)))) {
		debugController->EnableDebugLayer();
	}

	try { WINDOW_HANDLE = sample1::InitWindow(EXAMPLE_NAME, inst, show_cmd, WIDTH, HEIGHT, FULLSCREEN); }
	CATCH_EXCEPTS

	EnableDebugLayer();

	try { InitD3D12(); }
	CATCH_EXCEPTS

	StartLoop(&Init, &Render);

	ReportLiveObjects();

	return 0;
}
