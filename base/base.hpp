#pragma once

#include <windows.h>
#include <string>

#include <d3d12.h>
#include <d3dcompiler.h>
#include <dxgi1_5.h>
#include <array>
#include <variant>

#ifdef _DEBUG
#include <dxgidebug.h>
#include <wrl.h>
#endif

#include "d3dx12.h"

namespace tut {

struct CPU_DESCRIPTOR_HANDLE : public D3D12_CPU_DESCRIPTOR_HANDLE
{
	CPU_DESCRIPTOR_HANDLE() {}
	/* implicit */ CPU_DESCRIPTOR_HANDLE(const D3D12_CPU_DESCRIPTOR_HANDLE &o) :
		D3D12_CPU_DESCRIPTOR_HANDLE(o)
	{}
	CPU_DESCRIPTOR_HANDLE(CD3DX12_DEFAULT) { ptr = 0; }
	CPU_DESCRIPTOR_HANDLE(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE &other, INT offsetScaledByIncrementSize)
	{
		InitOffsetted(other, offsetScaledByIncrementSize);
	}
	CPU_DESCRIPTOR_HANDLE(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE &other, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		InitOffsetted(other, offsetInDescriptors, descriptorIncrementSize);
	}
	CPU_DESCRIPTOR_HANDLE& Offset(INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		ptr += offsetInDescriptors * descriptorIncrementSize;
		return *this;
	}
	CPU_DESCRIPTOR_HANDLE& Offset(INT offsetScaledByIncrementSize)
	{
		ptr += offsetScaledByIncrementSize;
		return *this;
	}
	bool operator==(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE& other) const
	{
		return (ptr == other.ptr);
	}
	bool operator!=(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE& other) const
	{
		return (ptr != other.ptr);
	}
	CPU_DESCRIPTOR_HANDLE &operator=(const D3D12_CPU_DESCRIPTOR_HANDLE &other)
	{
		ptr = other.ptr;
		return *this;
	}

	inline void InitOffsetted(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE &base, INT offsetScaledByIncrementSize)
	{
		InitOffsetted(*this, base, offsetScaledByIncrementSize);
	}

	inline void InitOffsetted(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE &base, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		InitOffsetted(*this, base, offsetInDescriptors, descriptorIncrementSize);
	}

	static inline void InitOffsetted(_Out_ D3D12_CPU_DESCRIPTOR_HANDLE &handle, _In_ const D3D12_CPU_DESCRIPTOR_HANDLE &base, INT offsetScaledByIncrementSize)
	{
		handle.ptr = base.ptr + offsetScaledByIncrementSize;
	}

	static inline void InitOffsetted(_Out_ D3D12_CPU_DESCRIPTOR_HANDLE &handle, _In_ const D3D12_CPU_DESCRIPTOR_HANDLE &base, INT offsetInDescriptors, UINT descriptorIncrementSize)
	{
		handle.ptr = base.ptr + offsetInDescriptors * descriptorIncrementSize;
	}
};

template<typename T, int size>
struct Vector {
	Vector(T x, T y) : x(x), y(y) {};

	union {
		struct { T x, y; };
		std::array<T, size> data;
	};
};

using IVec2 = Vector<std::uint16_t, 2>;

struct Texture
{
	ID3D12Resource* resource;
	ID3D12Resource* upload_resource;
	size_t bytes_per_row;
	std::uint8_t* address;
	D3D12_RESOURCE_DESC texture_desc;
};

class ExampleBase {
public:
	ExampleBase() = default;
	virtual ~ExampleBase();

	virtual void Render() = 0;
	virtual void Init() = 0;

	static LRESULT CALLBACK WindowProc(HWND handle, UINT msg, WPARAM w_param, LPARAM l_param);

	void SetupWindow(HINSTANCE inst, int show_cmd);
	void SetupD3D12();
	void SetupImGui();
	void SetupSwapChain();

	IVec2 GetWindowSize();

	void StartLoop();

	static std::uint32_t rtv_increment_size;
	static std::uint32_t dsv_increment_size;
	static std::uint32_t cbv_srv_uav_increment_size;
	static std::uint32_t sampler_increment_size;

	unsigned int frame_idx;

protected:
	IDXGIFactory5* factory = nullptr;
	ID3D12Device* device = nullptr;
	IDXGIAdapter1* adapter = nullptr;
	IDXGISwapChain4* swap_chain = nullptr;
	ID3D12CommandQueue* cmd_queue = nullptr;

#ifdef _DEBUG
	Microsoft::WRL::ComPtr<ID3D12Debug> debug_controller;
#endif
	ID3D12DescriptorHeap* imgui_descriptor_heap = nullptr;

	static const std::string name;
	static const bool allow_fullscreen;
	static const bool allow_resizing;
	static const std::uint16_t initial_width;
	static const std::uint16_t initial_height;
	static const std::uint8_t num_backbuffers;
	static const D3D_FEATURE_LEVEL feature_level;

	HWND window_handle;
};

template<typename T>
void SafeRelease(T* object) {
	object->Release();
}

[[nodiscard]] inline unsigned int SizeOfFormat(const DXGI_FORMAT format)
{
switch (format)
	{
	case DXGI_FORMAT_R32G32B32A32_TYPELESS:
	case DXGI_FORMAT_R32G32B32A32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_UINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:
		return 16;

	case DXGI_FORMAT_R32G32B32_TYPELESS:
	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32_SINT:
		return 12;

	case DXGI_FORMAT_R16G16B16A16_TYPELESS:
	case DXGI_FORMAT_R16G16B16A16_FLOAT:
	case DXGI_FORMAT_R16G16B16A16_UNORM:
	case DXGI_FORMAT_R16G16B16A16_UINT:
	case DXGI_FORMAT_R16G16B16A16_SNORM:
	case DXGI_FORMAT_R16G16B16A16_SINT:
	case DXGI_FORMAT_R32G32_TYPELESS:
	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32_SINT:
	case DXGI_FORMAT_R32G8X24_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
	case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
		return 8;

	case DXGI_FORMAT_R10G10B10A2_TYPELESS:
	case DXGI_FORMAT_R10G10B10A2_UNORM:
	case DXGI_FORMAT_R10G10B10A2_UINT:
	case DXGI_FORMAT_R11G11B10_FLOAT:
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
	case DXGI_FORMAT_R8G8B8A8_UINT:
	case DXGI_FORMAT_R8G8B8A8_SNORM:
	case DXGI_FORMAT_R8G8B8A8_SINT:
	case DXGI_FORMAT_R16G16_TYPELESS:
	case DXGI_FORMAT_R16G16_FLOAT:
	case DXGI_FORMAT_R16G16_UNORM:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16_SNORM:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R32_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT:
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R24G8_TYPELESS:
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
	case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
	case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
	case DXGI_FORMAT_B8G8R8A8_UNORM:
	case DXGI_FORMAT_B8G8R8X8_UNORM:
		return 4;

	case DXGI_FORMAT_R8G8_TYPELESS:
	case DXGI_FORMAT_R8G8_UNORM:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8_SNORM:
	case DXGI_FORMAT_R8G8_SINT:
	case DXGI_FORMAT_R16_TYPELESS:
	case DXGI_FORMAT_R16_FLOAT:
	case DXGI_FORMAT_D16_UNORM:
	case DXGI_FORMAT_R16_UNORM:
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16_SNORM:
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_B5G6R5_UNORM:
	case DXGI_FORMAT_B5G5R5A1_UNORM:
		return 2;

	case DXGI_FORMAT_R8_TYPELESS:
	case DXGI_FORMAT_R8_UNORM:
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8_SNORM:
	case DXGI_FORMAT_R8_SINT:
	case DXGI_FORMAT_A8_UNORM:
		return 1;

	// Compressed format; http://msdn2.microsoft.com/en-us/library/bb694531(VS.85).aspx
	case DXGI_FORMAT_BC2_TYPELESS:
	case DXGI_FORMAT_BC2_UNORM:
	case DXGI_FORMAT_BC2_UNORM_SRGB:
	case DXGI_FORMAT_BC3_TYPELESS:
	case DXGI_FORMAT_BC3_UNORM:
	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC5_TYPELESS:
	case DXGI_FORMAT_BC5_UNORM:
	case DXGI_FORMAT_BC5_SNORM:
		return 16;

	// Compressed format; http://msdn2.microsoft.com/en-us/library/bb694531(VS.85).aspx
	case DXGI_FORMAT_R1_UNORM:
	case DXGI_FORMAT_BC1_TYPELESS:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
	case DXGI_FORMAT_BC4_TYPELESS:
	case DXGI_FORMAT_BC4_UNORM:
	case DXGI_FORMAT_BC4_SNORM:
		return 8;

	// Compressed format; http://msdn2.microsoft.com/en-us/library/bb694531(VS.85).aspx
	case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
		return 4;

	// These are compressed, but bit-size information is unclear.
	case DXGI_FORMAT_R8G8_B8G8_UNORM:
	case DXGI_FORMAT_G8R8_G8B8_UNORM:
		return 4;

	case DXGI_FORMAT_UNKNOWN:
	default:
		return 0;
	}
}

[[nodiscard]] ID3D12DescriptorHeap* CreateDepthStencilHeap(ID3D12Device* device, std::uint16_t num_buffers);
[[nodiscard]] ID3D12Resource* CreateDepthStencilBuffer(ID3D12Device* device, CPU_DESCRIPTOR_HANDLE desc_handle, IVec2 size);
[[nodiscard]] ID3D12Resource* CreateDepthStencilBuffer(ID3D12Device* device, CPU_DESCRIPTOR_HANDLE desc_handle, std::uint16_t width, std::uint16_t height);
[[nodiscard]] ID3D12DescriptorHeap* CreateRenderTargetViewHeap(ID3D12Device* device, std::uint16_t num_buffers);

template<const std::uint16_t num>
[[nodiscard]] std::array<ID3D12Resource*, num> GetRenderTargetsFromSwapChain(ID3D12Device* device, IDXGISwapChain4* swap_chain) {
	std::array<ID3D12Resource*, num> render_targets;

	for (std::uint16_t i = 0; i < num; i++) {
		HRESULT hr = swap_chain->GetBuffer(i, IID_PPV_ARGS(&render_targets[i]));
		if (FAILED(hr)) {
			throw "Failed to get swap chain buffer.";
		}
		render_targets[i]->SetName(L"Back buffer");
	}

	return render_targets;
}

template<const std::uint16_t num>
void CreateRTVsFromResourceArray(ID3D12Device* device, std::array<ID3D12Resource*, num> render_targets, CPU_DESCRIPTOR_HANDLE desc_handle) {
	for (std::uint16_t i = 0; i < num; i++) {
		device->CreateRenderTargetView(render_targets[i], nullptr, desc_handle);
		desc_handle.Offset(1, ExampleBase::rtv_increment_size);
	}
}

[[nodiscard]] inline std::wstring GetUTF16(std::string_view const str, int codepage) {
	if (str.empty()) return std::wstring();
	int sz = MultiByteToWideChar(codepage, 0, &str[0], (int)str.size(), 0, 0);
	std::wstring retval(sz, 0);
	MultiByteToWideChar(codepage, 0, &str[0], (int)str.size(), &retval[0], sz);
	return retval;
}

[[nodiscard]] inline std::pair<ID3DBlob*, D3D12_SHADER_BYTECODE> LoadShader(std::string_view path, std::string_view entry, std::string_view type) {
	D3D_SHADER_MACRO defines[] = { "GPU", NULL, NULL, NULL };

	ID3DBlob* shader;
	ID3DBlob* error;
	HRESULT hr = D3DCompileFromFile(GetUTF16(path, CP_UTF8).c_str(),
		defines,
		nullptr,
		entry.data(),
		type.data(),
		D3DCOMPILE_OPTIMIZATION_LEVEL3,
		0,
		&shader,
		&error);
	if (FAILED(hr)) {
		auto msg = error->GetBufferPointer();
		MessageBox(nullptr, TEXT((char*)msg), NULL, MB_OK | MB_ICONERROR);
		std::exit(-1);
	}

	D3D12_SHADER_BYTECODE bytecode = {};
	bytecode.BytecodeLength = shader->GetBufferSize();
	bytecode.pShaderBytecode = shader->GetBufferPointer();

	return std::make_pair(shader, bytecode);
}

[[nodiscard]] inline std::variant<std::pair<ID3DBlob*, D3D12_SHADER_BYTECODE>, std::string> ReloadShader(std::string_view path, std::string_view entry, std::string_view type)
{
	D3D_SHADER_MACRO defines[] = { "GPU", NULL, NULL, NULL };

	ID3DBlob* shader;
	ID3DBlob* error;
	HRESULT hr = D3DCompileFromFile(GetUTF16(path, CP_UTF8).c_str(),
		defines,
		nullptr,
		entry.data(),
		type.data(),
		D3DCOMPILE_OPTIMIZATION_LEVEL3,
		0,
		&shader,
		&error);
	if (FAILED(hr))
	{
		return std::string((char*)error->GetBufferPointer());
	}

	D3D12_SHADER_BYTECODE bytecode = {};
	bytecode.BytecodeLength = shader->GetBufferSize();
	bytecode.pShaderBytecode = shader->GetBufferPointer();

	return std::make_pair(shader, bytecode);
}

/*! Create a texture where we can render to. */
static Texture* CreateRenderTexture(ID3D12Device* device, unsigned int width, unsigned int height, DXGI_FORMAT format, tut::CPU_DESCRIPTOR_HANDLE srv_handle)
{
	Texture* texture = new Texture();

	D3D12_RESOURCE_DESC texture_desc = {};
	texture_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	texture_desc.Alignment = 0;
	texture_desc.Width = width;
	texture_desc.Height = height;
	texture_desc.DepthOrArraySize = 1;
	texture_desc.MipLevels = 1;
	texture_desc.Format = format;
	texture_desc.SampleDesc.Count = 1;
	texture_desc.SampleDesc.Quality = 0;
	texture_desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	texture_desc.Flags = D3D12_RESOURCE_FLAG_NONE;
	texture->texture_desc = texture_desc;

	texture->bytes_per_row = width * tut::SizeOfFormat(format);

	size_t texture_upload_buffer_size;
	device->GetCopyableFootprints(&texture_desc, 0, 1, 0, nullptr, nullptr, nullptr, &texture_upload_buffer_size);

	CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(texture_upload_buffer_size);

	HRESULT hr = device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&texture_desc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texture->resource));

	if (FAILED(hr))
		throw std::runtime_error("Couldn't create default texture");

	hr = device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&desc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&texture->upload_resource));

	if (FAILED(hr))
		throw std::runtime_error("Couldn't create upload texture");

	texture->resource->SetName(L"Render Texture");
	texture->upload_resource->SetName(L"Upload Render Texture");

	// Create SRV View
	unsigned int increment_size = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	D3D12_SHADER_RESOURCE_VIEW_DESC srv_desc = {};
	srv_desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srv_desc.Format = texture->texture_desc.Format;
	srv_desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srv_desc.Texture2D.MipLevels = texture->texture_desc.MipLevels;
	srv_desc.Texture2D.MostDetailedMip = 0;

	device->CreateShaderResourceView(texture->resource, &srv_desc, srv_handle);

	return texture;
}

inline void UpdateRenderTexture(ID3D12GraphicsCommandList* cmd_list, Texture* texture, BYTE* data)
{
	ID3D12Device* n_device;
	cmd_list->GetDevice(IID_PPV_ARGS(&n_device));

	cmd_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texture->resource, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST));

	size_t texture_upload_buffer_size;
	size_t bytes_per_row;
	UINT num_rows;
	n_device->GetCopyableFootprints(&texture->texture_desc, 0, 1, 0, nullptr, &num_rows, &bytes_per_row, &texture_upload_buffer_size);

	D3D12_SUBRESOURCE_DATA image_data = {};
	image_data.pData = data;
	image_data.RowPitch = texture->bytes_per_row;
	image_data.SlicePitch = (bytes_per_row) * num_rows;

	UpdateSubresources(cmd_list, texture->resource, texture->upload_resource, 0, 0, 1, &image_data);
	cmd_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texture->resource, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM w_param, LPARAM l_param);

// Application Entry
#define APP_ENTRY(et) \
int WINAPI WinMain(HINSTANCE inst, HINSTANCE prev_inst, LPSTR cmd_line, int show_cmd) { \
	et* example = new et(); \
	example->SetupWindow(inst, show_cmd); \
	example->SetupD3D12(); \
	example->SetupImGui(); \
	example->SetupSwapChain(); \
	example->StartLoop(); \
	delete example; \
	return 0; \
}

} /* tut */